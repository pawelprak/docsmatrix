# Wstęp  
Instrukcja wewnętrzna  

GUL-500/350  
Komputer WK5  
**Wgrywanie systemu operacyjnego Linux TinyCore**  

Autor: Paweł Karp   

# Wersja  
v1.0.0 - 2015-09-16 - Stworzenie dokumentu.      
     

# Stworzenie bootowalnego pendrive  
Pobrać obraz systemu z lokalizacji:  
//192.168.30.12/oprogramowanie dla produkcji/Paweł Karp/08 - GUL/systemOperacyjnyWK5  

Programem Universal-USB-Installer stworzyć bootowalnego pendriva z zainstalowanym obrazem LinuxTinyCore:  

- Step 1 – wybrać Try Unlisted Linux ISO (na samym dole listy),  
- Step 2 – wskazać przegrany wcześniej plik z rozszerzeniem *.iso  
- Step 3 – Wybrać oznaczenie literowe  pendriva (np. F), zaznaczyć formatowanie pendrive’a,  
Zatwierdzić operację przyciskiem „Create” a następnie zatwierdzić ustawienia klikając przycisk „Tak”.  


# Instalacja
### Informacje początkowe  
Na komputerze WK5 system operacyjny Linux instalowany jest na dysku USB (nie na SSD jak to miało w przypadku systemu Windows Embedded).  
Jeśli jest włożony dysk SSD należy go wyciągnąć a zamiast niego włożyć dysk USB w złącze na płytce opisane jako *USB3*.   
Dysk ma oznaczenie: FDU-4G-XT (index materiałowy 568-343-0101).  

### Ustawienia BIOS
- Podłączyć do monitora DC1 klawiaturę oraz myszkę USB (poprzez przweód DSC 6981082),  
- Podłączyć uprzednio przygotowanego pendrive’a do gniazda USB w monitorze,  
- Podać zasilanie na monitor, podczas startu systemu wejść do ustawień BIOSu wciskając klawisz DELETE,  
- W BIOS odpowiednio poustawiać:  
    - Advanced / USB Configuration  
        - Legacy USB support - Enabled  
        - USB 2.0 Controller Mode - HighSpeed  
        - BIOS EHCI Hand-Off - Enabled  
        - USB Mass Storage Device Configuration - obydwa dyski usb mają być ustawione w trybie emulacji AUTO  
    - Boot / Boot Device Priority - Ustawić włożony pendrive na pierwszej pozycji bootowania  
    - Chipset / North Bridge Chipset Configuration  
        - Primary Graphics adapter - IGD  
        - Integrated Graphics Mode Selec - Enabled, 8MB  
        - Flat Panel Type - 800x600  
        - Boot Display Device - Integrated LVDS  
    - Chipset / South Bridge Chipset Configuration  
        - USB Functions - 6 USB Ports  
        - USB 2.0 Controller - Enabled  
        - HDA Controller - Disabled  
    - Exit / Save Changes and Exit  
    
        
### Start systemu z pendrive
- Po restarcie wystartuje system operacyjny zainstalowany na Pendrive  
- W pierwszym Menu, które się pokaże wybrać opcję „Default”    
- Po uruchomieniu się systemu otworzy się automatycznie przeglądarka internetowa Chromium, należy ją zamknąć kombinacją klawiszy ALT+F4,  
- Kliknąć prawym klawiszem myszy na pulpit i z menu wybrać Aplications/Terminal (lub SystemTools/Terminal) aby sformatować wewnętrzne dyski komputera.  
W terminalu wpisać komendy i każdą zatwierdzić klawiszem ENTER (uwaga: podkreślenia „_” należy zastąpić spacjami):  
`$ mkfs.ext4_/dev/sdb1`  

### Instalacja systemu z pendrive na DC1
Kliknąć prawym klawiszem myszy na pulpit i z menu wybrać Applications/tc-install.  
Uruchomi się program instalacyjny. Ustawienia na poszczególnych planszach (do każdej następnej planszy przechodzi się klikając czarną strzałkę w prawo):  

- Tiny Core Installation  
    *Path to core.gz* - kliknąć lewym przyciskiem myszy na pole wyboru. W okienku które się pojawi wybrać plik *core.gz* w tej lokalizacji:    
>/mnt / sda1 / boot / core.gz  
    
Wybór zaakceptować przyciskiem OK.  
Zaznaczyć opce Frugal oraz Whole Disk.   
Select disk for core: sdb  
Zaznaczyć Install boot loader  

- Formatting Options: ext4  
- Boot Options Reference List,  wpisać odpowiednie polecenia (uwaga: podkreślenia „_” należy zastąpić spacjami):  
>logo.nologo_console=ttyS0_vga=789_noicons  

- Extension Installation:  kliknąć pole wyboru i wskazać na 
>/mnt/sda1/cde  (Uwaga: należy wskazać katalog cde a nie jego wnętrze)

- Review: Nacisnąć przycisk Proceed.  

Rozpocznie się instalacja systemu operacyjnego.  
Koniec instalacji zostanie po wyświetleniu komunikatu „Installation has completed”.  


### Ponowne uruchomienie monitora
Wyciągnąć pendrive z monitora.  
Kliknąć prawym klawiszem myszy na pulpit i z menu wybrać „Exit”, zaznaczyć Reboot oraz w Backup Options z rozwijalnej listy wybrać „None”.  
Kliknięcie przycisku OK zrestartuje system.  

Wejść ponownie  w BIOS gdzie należy ustawić odpowiednią kolejność bootowania.  
Uruchomi się system z wewnętrznego dysku monitora.  


### Pozbycie się paska z ikonkami
Prawy przycisk myszy na pulpicie (PPM):  
Apps/Maintenance/OnBoot Maintenance - zaznaczyć program *wbar*  z listy po prawej stronie - On Boot Items (onboot.lst).  
Nacisnąć przycisk *Delete Item from list*.  
PPM/Exit/Reboot - Backup options: Backup.  


# Sprawdzenie funkcjonalności systemu po nowej instalacji
### Połączenie SFTP
Połączyć laptopa z monitorem kablem ethernetowym.  
Na laptopie ustawić adres 192.168.3.66 i maskę sieci 255.255.255.0.  
Sprawdzić z konsoli poleceniem ping czy laptop widzi komputer DC1 (ping 192.168.3.31).  

Test należy wykonać programem FileZilla.  
W Menadżerze Stron skonfigurować nowe połączenie z następującymi ustawieniami i wykonać probe połącznia:  

>Serwer: 192.168.3.31  
Protokół: SFTP – SSH File Transfer Protocol  
Typ logowania: Normalna  
Użytkownik: tc  
Hasło: wihajster1234     

Uwaga: Podłączenie do serwera może trwać nawet kilkanaście sekund.  
	

### Połączenie VNC  
Programem TightVNC Viewer wykonać próbę podłączenia do monitora.  
Serwer VNC nie wymaga hasła.  


# Wgranie aktualnego oprogramowania  
### Wizualizacja  
Uwaga: instrukcja nie dotyczy aktualizacji serwera www od Arka.  

Aktualizacja będzie wykonowane za pomocą programu Filezilla (protokół SFTP).  
Przegrać najnowszy program wizualizacyjny z lokalizacji:  

>192.168.30.12/oprogramowanie dla produkcji/Kombajny/Chodnikowe -> wybrać odpowiednią kopalnię.  

Należy skopiować cały katalog „build” na laptop z którego będzie wgrywany program.  
Włączyć zasilanie na DC1 o poczekać aż uruchomi się wizualizacja.  
Połączyć się Filezillą. Po wylistowaniu plików na DC1 przejść do katalogu *kopex*.  
Skasować cały katalog *build* i przegrać w jego miejsce ten pobrany z serwera 192.168.30.12.  
Dodatkowo z katalogu *build/_json* przegrać całą zawartość do *kopex/json*.  

**Restart systemu v1**:  
Jeśli posiadamy podpiętą klawiaturę i myszkę.  
PPM/Exit/Reboot - Backup options: Backup.   

**Restart systemu v2**:  
Tylko za pomocą sftp.  
Zainstalować na laptopie progam PuTTY.   
Można go pobrać np. ze strony [**http://www.chiark.greenend.org.uk/**](http://www.chiark.greenend.org.uk/)  
Uruchomić program w następującej konfiguracji:  

>Host Name (or IP adress) - 192.168.3.31 / Port: 22  
Connection type - SSH  
OPEN  

Po nawiązaniu połączenia:  
`login as: tc`  (nawa użytkownika)  
`tc@192.168.3.31's password: wihajster1234`  (hasło użytkownika)  
`tc@box:$ filetool.sh -b`  (zostają zapisane zmiany dokonane w systemie)  
`tc@box:$ sudo reboot`  (restart systemu)   







