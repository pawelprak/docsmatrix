# Wstęp
Instrukcja wewnętrzna  

**Aktualizacja oprogramowania wizualizacyjnego przez protokół SFTP**  

Autor: Paweł Karp 


# Wersja
v1.0 - 2015-12-02 - Stworzenie dokumentu  


# Przygotowanie laptopa  
### Ustawienie adresu IP  
Na laptopie ustawić adres statyczny 192.168.3.66 i maskę sieci 255.255.255.0    
Połączyć laptopa z monitorem kablem ethernetowym.  
Sprawdzić z konsoli poleceniem ping czy laptop widzi monitor, na którym będzie wgrywane oprogramowanie:  
`ping 192.168.3.31`  - monitor główny (WK5 na GUL, DC1 na KTW)  
`ping 192.168.3.51`  - monitor pomocniczy (Olimex na KTW)   


### Filezilla  
Ściagnąć z internetu program filezilla i go zainstalować.  
W jego ustawieniach należy zwiększyć czas oczekiwania:  

>Edytuj / Ustawienia / Połączenie / Limit czasu oczekiwania w sekundach: 60  
     
W Menadżerze Stron skonfigurować nowe połączenie z następującymi ustawieniami i wykonać próbę połącznia:  

>Serwer: 192.168.3.31  
Protokół: SFTP – SSH File Transfer Protocol  
Typ logowania: Normalna  
Użytkownik: *tc* (dla linuxa tinyCore), *zzm* (dla olimex)  
Hasło: wihajster1234  

Uwaga: Podłączenie do serwera może trwać nawet kilkanaście sekund.  


### PuTTY  
Zainstalować na laptopie progam PuTTY.   
Można go pobrać np. ze strony [**http://www.chiark.greenend.org.uk/**](http://www.chiark.greenend.org.uk/)  
Uruchomiać program w następującej konfiguracji:  

>Host Name (or IP adress) - 192.168.3.31 (albo 192.168.3.51 dla monitora pomocniczego) / Port: 22  
Connection type - SSH  
OPEN  


# Aktualizacja oprogramowania  
### Serwer www
Serwer przegrać do katalogu /home/nazwa_użytkownika/kopex/ gdzie:  
nazwa_użytkownika - na tinycore: *tc*, olimexie: *zzm*  
Po szczegółową instrukcję zwrócić się do Arka H.  


### Wizualizacja
Aktualizacja będzie wykonowane za pomocą programu Filezilla.  
Przegrać najnowszy program wizualizacyjny z lokalizacji:  

>192.168.30.12 / oprogramowanie dla produkcji / Kombajny/ -> wybrać odpowiedni typ kombajnu i kopalnię.  

Należy skopiować cały katalog „build” na laptop z którego będzie wgrywany program.  
Włączyć zasilanie na monitorze kombajnowym o poczekać aż uruchomi się wizualizacja.  
Połączyć się Filezillą.  
Po wylistowaniu plików znajdujących się na monitorze przejść do katalogu *kopex*.  
Skasować cały katalog *build* i przegrać w jego miejsce ten pobrany z serwera 192.168.30.12.  
Dodatkowo z katalogu *build/_json* przegrać całą zawartość do *kopex/json*.  


# Restart systemu
### Monitory DC1, WK5  
Na tych monitorach, na których jest zainstalowany system linux TinyCore wszystkie wprowadzone zmiany muszą być zapamietane poprzez specjalną komendę.  

**Restart systemu v1**:  
Jeśli posiadamy podpiętą klawiaturę i myszkę. 

>PPM / Exit  /Reboot - Backup options: Backup.   

**Restart systemu v2**:  
Połączyć się progamem PuTTY.   
Po nawiązaniu połączenia:  
`login as: tc`  (nazwa użytkownika)  
`tc@192.168.3.31's password: wihajster1234`  (hasło użytkownika, może też wystąpić samo `wihajster`)  
`tc@box:$ filetool.sh -b`  (zostają zapisane zmiany dokonane w systemie)  
`tc@box:$ sudo reboot`  (restart systemu)  


### Monitory Olimex  
Połączyć się progamem PuTTY.   
Po nawiązaniu połączenia:  
`login as: zzm`  (nazwa użytkownika)  
`zzm@192.168.3.31's password: wihajster1234`   
`zzm@box:$ sudo reboot`  (restart systemu)  






