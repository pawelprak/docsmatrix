# Wstęp  
Instrukcja wewnętrzna  

KTW-150/200  
Komputer Mikro Elektronik DC1    
**Wgrywanie systemu operacyjnego Linux TinyCore**  

Autor: Paweł Karp   

# Wersja  
v1.3.0 - 2015-07-17 - Przeniesiono na GIT.    
v1.3.1 - 2015-08-05 - Dodano sposób zdalnego restartu systemu (po sftp).  
v1.4.0 - 2015-12-03 - Wydzielenie sposobu aktualizacji wizualizacji do oddzielnego dokumentu. 


# Stworzenie bootowalnego pendrive  
Pobrać obraz systemu z lokalizacji:  
\\192.168.30.12\Oprogramowanie dla produkcji\Paweł Karp\07-Nowy KTW\System operacyjny monitora ognioszczelnego DC1.   

Programem Universal-USB-Installer stworzyć bootowalnego pendriva z zainstalowanym obrazem LinuxTinyCore:  

- Step 1 – wybrać Try Unlisted Linux ISO (na samym dole listy),  
- Step 2 – wskazać przegrany wcześniej plik z rozszerzeniem *.iso  
- Step 3 – Wybrać oznaczenie literowe  pendriva (np. F), zaznaczyć formatowanie pendrive’a,  
Zatwierdzić operację przyciskiem „Create” a następnie zatwierdzić ustawienia klikając przycisk „Tak”.  


# Instalacja
### Ustawienia BIOS
- Podłączyć do monitora DC1 klawiaturę oraz myszkę USB (można poprzez HUB USB),  
- Podłączyć uprzednio przygotowanego pendrive’a do gniazda USB w monitorze,  
- Podać zasilanie na monitor, podczas startu systemu wejść do ustawień BIOSu wciskając klawisz DELETE,  
- W BIOS odpowiednio poustawiać:  
    - Load Optimized Defaults  
    - Standard CMOS Features / IDE Channel 0 Master (wejść do menu wyboru wciskając ENTER) / None  
    - Standard CMOS Features / IDE Channel 0 Slave (wejść do menu wyboru wciskając ENTER) / None  
    - Advanced BIOS features / Hard Disk Boot Priority / Ustawić następującą kolejność:  
        1. Nasz pendrive z systemem  
        2. USB-HDD0: eUSB	 
        3. USB-HDD1: eUSB  
        4. Bootable Add-in Cards  
        Boot priority: Manual  
        Uwaga: (nazwy dysków eUSB są różne w zależności od wersji DC1)  
    - Advanced BIOS features :  
        - First Boot Device		- Hard Disk  
        - Second Boot Device	- Disabled  
        - Third Boot Device		- Disabled  
        - LAN Boot ROM			- Disabled  
    - Integrated Peripherals / OnChip IDE Device  
        - IDE Primary Master UDMA	- Disabled  
        - IDE Primary Slave UDMA	- Disabled  
        - Delay For HDD (Secs)		- 0  
    - Integrated Peripherals / Onboard Device  
        - Intel HD Audio Controller	- Disabled  
    - Integrated Peripherals / USB Device Settings:  
        - USB 1.0 Controller	- Enabled  
        - USB 2.0 Controller	- Enabled  
        - USB Operation Mode	- High Speed  
        - USB Storage Function	- Enabled  
        - ||||||||||||  
        - eUSB SSD			           - HDD mode  
        - eUSB SSD			           - HDD mode  
        - pendrive z naszym systemem   - HDD mode  
    - Advanced Chipset Features  
        - System BIOS Cacheable	- Disabled  
    - Frequency/Voltage Control / Spread Spectrum	- Disabled  
    - Save & Exit Setup / Y  
    
### Start systemu z pendrive
- Po restarcie wystartuje system operacyjny zainstalowany na Pendrive  
- W pierwszym Menu, które się pokaże wybrać opcję „Boot TinyCore”    
- Po uruchomieniu się systemu otworzy się automatycznie przeglądarka internetowa Chromium, należy ją zamknąć kombinacją klawiszy ALT+F4,  
- Kliknąć prawym klawiszem myszy na pulpit i z menu wybrać Aplications/Terminal (lub SystemTools/Terminal) aby sformatować wewnętrzne dyski komputera.  
W terminalu wpisać komendy i każdą zatwierdzić klawiszem ENTER (uwaga: podkreślenia „_” należy zastąpić spacjami):  
`$ mkfs.ext4_/dev/sdb1`  
`$ mkfs.ext4_/dev/sdc1`  

### Instalacja systemu z pendrive na DC1
Kliknąć prawym klawiszem myszy na pulpit i z menu wybrać Applications/tc-install.  
Uruchomi się program instalacyjny. Ustawienia na poszczególnych planszach (do każdej następnej planszy przechodzi się klikając czarną strzałkę w prawo):  

- Tiny Core Installation  
    *Path to core.gz* - kliknąć lewym przyciskiem myszy na pole wyboru. W okienku które się pojawi wybrać plik *core.gz* w tej lokalizacji:    
>/mnt / sda1 / boot / core.gz  
    
Wybór zaakceptować przyciskiem OK.  
Zaznaczyć opce Frugal oraz Whole Disk.   
Select disk for core: sdb  
Zaznaczyć Install boot loader  

- Formatting Options: ext4  
- Boot Options Reference List,  wpisać odpowiednie polecenia (uwaga: podkreślenia „_” należy zastąpić spacjami):  
>quiet _logo.nologo_console=ttyS0_vga=792_noicons  


- Extension Installation:  kliknąć pole wyboru i wskazać na 
>/mnt/sda1/cde  (Uwaga: należy wskazać katalog cde a nie jego wnętrze)

- Review: Nacisnąć przycisk Proceed.  

Rozpocznie się instalacja systemu operacyjnego.  
Koniec instalacji zostanie po wyświetleniu komunikatu „Installation has completed”.  


### Ponowne uruchomienie monitora
Wyciągnąć pendrive z monitora.  

Kliknąć prawym klawiszem myszy na pulpit i z menu wybrać „Exit”, zaznaczyć Reboot oraz w Backup Options z rozwijalnej listy wybrać „None”. Kliknięcie przycisku OK zrestartuje system.  

Wejść ponownie  w BIOS gdzie należy ustawić odpowiednią kolejność bootowania.  

- Advanced BIOS Features  
     - Hard Disk Boot Priority -> ustawić na pozycji  1 dysk z zainstalowanym systemem (HDD0 lub HDD1).  
- Wyjść i zapisać zmiany klawiszem F10.  

Uruchomi się system z wewnętrznego dysku monitora.  
W przypadku wystąpienia błędu spróbować uruchomić system z drugiego dysku (zmienić ustawienia kolejności bootowania w BIOS).  


### Pozbycie się paska z ikonkami
Prawy przycisk myszy na pulpicie (PPM):  
Apps/Maintenance/OnBoot Maintenance - zaznaczyć program *wbar*  z listy po prawej stronie - On Boot Items (onboot.lst).  
Nacisnąć przycisk *Delete Item from list*.  
PPM/Exit/Reboot - Backup options: Backup.  


# Sprawdzenie funkcjonalności systemu po nowej instalacji
### Połączenie SFTP
Połączyć laptopa z monitorem kablem ethernetowym.  
Na laptopie ustawić adres 192.168.3.66 i maskę sieci 255.255.255.0.  
Sprawdzić z konsoli poleceniem ping czy laptop widzi komputer DC1 (ping 192.168.3.31).  

Test należy wykonać programem FileZilla.  
W jego ustawieniach należy zwiększyć czas oczekiwania:  

>Edytuj / Ustawienia / Połączenie / Limit czasu oczekiwania w sekundach: 60  

W Menadżerze Stron skonfigurować nowe połączenie z następującymi ustawieniami i wykonać probe połącznia:  

>Serwer: 192.168.3.31  
Protokół: SFTP – SSH File Transfer Protocol  
Typ logowania: Normalna  
Użytkownik: tc  
Hasło: wihajster1234  

Uwaga: Podłączenie do serwera może trwać nawet kilkanaście sekund.  
	

### Połączenie VNC  
Programem TightVNC Viewer wykonać próbę podłączenia do monitora.  
Serwer VNC nie wymaga hasła.  


# Wgranie aktualnego oprogramowania  
### Wizualizacja  
Użyć osobnej instrukcji.  


### Obrócenie ekranu o 180 stopni  
Podczas prac uruchomieniowych na hali wyszła wada ze słabymi kątami widzenia monitora DC1 od góry.   
Aby temu zaradzić należy programowo odwrócić wyświetlanie o 180 stopni.   
Programem mc zaznaczyć plik /home/tc/*.xsession i wcisnąć klawisz F4.   
Po pojawieniu się edytora należy zmienić fragment w pierwszej linijce z 1024x768x32 na 1024x768@180x32 (dodany fragment „@180” decyduje o orientacji monitora).  
Klawiszem F2 zapisujemy wprowadzone zmiany w pliku a następnie klawiszem F10 wychodzimy z edytora (zamykamy plik).   
Restart systemu: PPM/Exit/Reboot - Backup options: Backup.  






















