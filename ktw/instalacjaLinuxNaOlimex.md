# Wstęp  
Instrukcja wewnętrzna  

KTW-150/200  
Monitor pomocniczy (na Skrzyni Aparatury Elektrycznej)     
**Wgrywanie systemu operacyjnego Linux **  

Autor: Paweł Karp   

# Wersja  
v1.0.0 - 2015-12-05 - Stworzenie dokumentu.    



# Instalacja 
### Instalacja systemu na karcie SD  
Pobrać obraz systemu z lokalizacji:  
\\192.168.30.12\Oprogramowanie dla produkcji\Paweł Karp\07-Nowy KTW\System operacyjny monitora pomocniczego SAE    

Programem Win32 Disk Imager zapisać pobrany obraz na karcie SD 8GB.  
Umieścić kartę w komputerze olimex i podać napięcie zasilania.  


### Zmiana rozmiaru partycji  
Zainstalowany system operacyjny ma rozmiar 2GB (na takiej karcie SD był tworzony).  
Po wgraniu go na kartę 8GB należy roszeszyć parycję z 2 do 8GB.  

Jeśli otworzy się przeglądarka zamknąć ją kombinacją klawiszy ALT+F4.  
Uruchomić konsolę, wpisać:  
`$ sudo fdisk /dev/mmcblk0`  - uruchomienie programu fdisk  
Przebieg zmianu rozmiaru partycji:  

>Welcome to fdisk (util-linux 2.23.2).  
>  
>Changes will remain in memory only, until you decide to write them.  
>Be careful before using the write command.  
>  
>  
>**Command (m for help): d**  
>Selected partition 1  
>Partition 1 is deleted  
>
>**Command (m for help): n**  
>Partition type:  
>   p   primary (0 primary, 0 extended, 4 free)  
>   e   extended  
>Select (default p): **ENTER**   
>Using default response p  
>Partition number (1-4, default 1): **ENTER**     
>First sector (2048-15564799, default 2048):   **ENTER**  
>Using default value 2048  
>Last sector, +sectors or +size{K,M,G} (2048-15564799, default 15564799): **ENTER**    
>Using default value 15564799  
>Partition 1 of type Linux and of size 7.4 GiB is set  
>  
>**Command (m for help): w**    
>The partition table has been altered!  
>  
>Calling ioctl() to re-read partition table.  
>  
>WARNING: Re-reading the partition table failed with error 16: Device or resource busy.  
>The kernel still uses the old table. The new table will be used at  
>the next reboot or after you run partprobe(8) or kpartx(8)  
>Syncing disks.  

Restart systemu:  
`$ sudo shutdown -r now`  

Po restarcie zmienić rozmiar systemu:  
`$ sudo resize2fs /dev/mmcblk0p1`  

Sprawdzić ilość wolnego miejsca:  
`$ df -h`  
Powinno być około 8GB, zajęte około 25%.    


# Wgranie aktualnego oprogramowania wizualizacji  
Użyć osobnej instrukcji. 